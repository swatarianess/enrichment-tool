
# Enrichment Tool

This python application enriches financial transaction data by integrating additional entity information from an external API and calculating transaction costs based on the associated country.


## Features

- Reads CSV file from data directory (`data/input/`)
- Outputs CSV file to data directory (`data/output/`)
- Batch lei retrieval from external api


## Run Locally

Go to the project directory

```
$ cd enrichment-tool
```

Install and activate venv (Optional)
```
$ python3 -m venv .venv
$ source .venv/bin/activate
```

Install dependencies

```
$ pip install -r requirements.txt
```

Ensure the input file is in the correct directory;
```
$ data/input/input_dataset.csv
```

Run the tool

```
$ python main.py --input_file input_dataset.csv
```


## Running Tests

To run tests, run the following command

```bash
  pytest ./
```


## Usage

```bash
$ python main.py --help
usage: main.py [-h] [--input_file INPUT_FILE]

Process some financial data.

optional arguments:
  -h, --help            show this help message and exit
  --input_file INPUT_FILE
                        Specify the input CSV file. Default is "input_dataset.csv".
```