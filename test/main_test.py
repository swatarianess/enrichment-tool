import pandas as pd
from main import (
    fetch_api_data,
    enrich_dataframe,
    calculate_transaction_costs,
    transaction_cost
)

# Constants
API_URL = "https://api.gleif.org/api/v1/lei-records"


def test_fetch_all_lei_data(requests_mock, api_response):
    leis = ['XKZZ2JZF41MRHTR1V493']
    requests_mock.get(f"{API_URL}?filter[lei]={','.join(leis)}", json=api_response)
    result = fetch_api_data(leis, API_URL)
    assert result['XKZZ2JZF41MRHTR1V493']['entity']['legalName']['name'] == "CITIGROUP GLOBAL MARKETS LIMITED"


def test_enrich_dataframe(test_dataframe):
    lei_data = {
        "XKZZ2JZF41MRHTR1V493": {
            "entity": {
                "legalName": {"name": "CITIGROUP GLOBAL MARKETS LIMITED", "language": "en"},
                "legalAddress": {"country": "GB"}
            },
            "bic": ["SBILGB2LXXX"]
        }
    }
    enriched_df = enrich_dataframe(test_dataframe, lei_data)
    assert enriched_df.at[0, 'legalName_name'] == "CITIGROUP GLOBAL MARKETS LIMITED"
    assert enriched_df.at[0, 'bic'] == "SBILGB2LXXX"


def test_transaction_cost():
    row_gb = pd.Series({
        'legalAddress_country': 'GB',
        'notional': 100000,
        'rate': 0.05
    })

    row_nl = pd.Series({
        'legalAddress_country': 'NL',
        'notional': 200000,
        'rate': 0.03
    })

    result_gb = transaction_cost(row_gb)
    result_nl = transaction_cost(row_nl)

    assert result_gb == 100000 * 0.05 - 100000
    assert result_nl == abs(200000 * (1 / 0.03) - 200000)


def test_calculate_transaction_costs(test_dataframe):
    calculated_df = calculate_transaction_costs(test_dataframe)
    assert calculated_df.at[0, 'transaction_costs'] == (100000 * 0.05) - 100000
    assert calculated_df.at[1, 'transaction_costs'] == abs(200000 * (1 / 0.03) - 200000)
