import pytest
import pandas as pd


@pytest.fixture
def api_response():
    """Sample API response for testing."""
    return {
        "data": [
            {
                "attributes": {
                    "lei": "XKZZ2JZF41MRHTR1V493",
                    "entity": {
                        "legalName": {"name": "CITIGROUP GLOBAL MARKETS LIMITED", "language": "en"},
                        "legalAddress": {"country": "GB"}
                    },
                    "bic": ["SBILGB2LXXX"]
                }
            }
        ]
    }


@pytest.fixture
def test_dataframe():
    """Sample DataFrame for testing."""
    data = {
        'lei': ['XKZZ2JZF41MRHTR1V493', '213800MBWEIJDM5CU638'],
        'notional': [100000, 200000],
        'rate': [0.05, 0.03],
        'legalAddress_country': ['GB', 'NL']
    }
    return pd.DataFrame(data)
