import os

import numpy as np
import pandas as pd
import requests
import logging
import argparse

from typing import List


logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')


def read_data(filename):
    logging.info('Reading data from {}'.format(filename))
    return pd.read_csv(os.path.join("data/input/", filename))


def fetch_api_data(leis: List[str], api_url: str):
    """
    Fetch data for multiple LEIs from the API.
    :param leis: List of LEIs to filter for
    :param api_url: The URL of the GLEI API
    :return: Returns a list of data for each LEI
    """
    leis_param = ','.join(leis)
    response = requests.get(f"{api_url}?filter[lei]={leis_param}")
    if response.status_code == 200:
        data = response.json()['data']
        return {item['attributes']['lei']: item['attributes'] for item in data}
    else:
        logging.error(f"API request failed")
        return {}


def enrich_dataframe(df, lei_data):
    """
    Enrich the DataFrame using pre-fetched LEI data.
    """
    df['legalName_name'] = df['lei'].apply(lambda x: lei_data[x]['entity']['legalName']['name'] if x in lei_data else np.nan)
    df['legalName_language'] = df['lei'].apply(lambda x: lei_data[x]['entity']['legalName']['language'] if x in lei_data else np.nan)
    df['legalAddress_country'] = df['lei'].apply(lambda x: lei_data[x]['entity']['legalAddress']['country'] if x in lei_data else np.nan)
    df['bic'] = df['lei'].apply(lambda x: ";".join(lei_data[x].get('bic', [])) if x in lei_data else '')

    return df


def transaction_cost(row):
    country = row['legalAddress_country']
    if country == 'GB':
        return row['notional'] * row['rate'] - row['notional']
    elif country == 'NL':
        return abs(row['notional'] * (1 / row['rate']) - row['notional'])
    return np.nan


def calculate_transaction_costs(df):
    df['transaction_costs'] = df.apply(transaction_cost, axis=1)
    return df


def main(input_file):
    api_url = "https://api.gleif.org/api/v1/lei-records"
    output_file = "enriched_dataset.csv"

    df = read_data(input_file)
    unique_leis = df['lei'].unique()

    lei_data = fetch_api_data(unique_leis, api_url)

    # Phase 1 enrichment
    enriched_df = enrich_dataframe(df, lei_data)

    # Phase 2 enrichment
    enriched_df_with_transactions = calculate_transaction_costs(enriched_df)

    enriched_df_with_transactions.to_csv(os.path.join("data/output/", output_file), index=False)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Process some financial data.")
    parser.add_argument('--input_file', type=str, default='input_dataset.csv',
                        help='Specify the input CSV file. Default is "input_dataset.csv".')
    args = parser.parse_args()

    main(args.input_file)
